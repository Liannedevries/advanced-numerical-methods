%% Homework exercise Advanced Numerical Methods exercise 4

% Initial conditions and parameters
u0l = 1;        % IC for x < 0
u0r = 0;        % IC for x > 0
al = 2;
ar = 1;
Ncells = 12;     % number of cells on either side of origin
Ntimesteps = 10; % number of time steps
dt = 0.5;
dx = 1;
aup = [ones(1,Ncells)*al ones(1,Ncells)*ar];    % define a per grid cell to be used in upwind scheme

u = zeros(Ntimesteps+1,Ncells*2); % pre allocate u matrix
u(1,1:Ncells) = (ones(1,Ncells)*u0l);   % enter IC's in first row u
u(1,Ncells+1:end) = (ones(1,Ncells)*u0r);   % enter IC's in first row u
uup = zeros(Ntimesteps+1,Ncells*2); % pre allocate u matrix
uup(1,1:Ncells) = (ones(1,Ncells)*u0l);   % enter IC's in first row u
uup(1,Ncells+1:end) = (ones(1,Ncells)*u0r);   % enter IC's in first row u


%% a) Beam-Warming method
for n = 1:Ntimesteps
    for i = 1+2:Ncells  % perform 1 timestep on left side of origin (a = 2). Only for cells at least 2 more to the right than all initialized cells to prevent getting negative indices
        u(n+1,i) = u(n,i) - dt/(2*dx)*al*(3*u(n,i) - 4*u(n,i-1) + u(n,i-2))+(0.5*(dt/dx)^2)*(al^2)*(u(n,i) - 2*u(n,i-1) + u(n,i-2));
    end
    for i = Ncells:2*Ncells % performing timestep right of origin
        u(n+1,i) = u(n,i) - dt/(2*dx)*ar*(3*u(n,i) - 4*u(n,i-1) + u(n,i-2))+(0.5*(dt/dx)^2)*(ar^2)*(u(n,i) - 2*u(n,i-1) + u(n,i-2));
    end
    for i = 1:2     % setting values of endpoints to value it should be when infinite domain is assumed. Information only propagates to the right (a positive), so this always works
        u(n+1,i) = u(n+1,3);
    end
end

for k = 1:Ntimesteps+1
    bar(u(k,:))
    pause(0.2)
end

%% b) first order upwind method
for n = 1:Ntimesteps
    for i = 1+2:2*Ncells
        uup(n+1,i) = uup(n,i) - (dt/dx)*(aup(i)*uup(n,i) - aup(i-1)*uup(n,i-1));
    end
    for i = 1:2     % setting values of endpoints to value it should be when infinite domain is assumed. Information only propagates to the right (a positive), so this always works
        uup(n+1,i) = uup(n+1,3);
    end
end
%%
figure 
hold on
for k = 1:Ntimesteps+1
    bar(uup(k,:),'b')
    pause(0.2)
end

%% d) conservation of u
function [f] = fx(n)
% our fx function
f=linspace(-5+(10)/(2*n),5-(10)/(2*n),n);
for i=1:length(f)
    if f(i)>-3 && f(i)<-1
        f(i)=0.5*(1-cos(pi*(f(i)-1)));
    elseif f(i)>1 && f(i)<3
        f(i)=1;
    else
        f(i)=0;
    end
end 


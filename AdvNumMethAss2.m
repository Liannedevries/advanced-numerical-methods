% Global variables
lb=-5; % left bound of the domain
rb=5; % right bound of the domain
N=500; % number of cells in x
h=0.001; % timestep
H=700; %number of time steps


T=h*H; %final time
n=(rb-lb)/(N); %Cell size

%%

x=linspace(lb+(rb-lb)/(2*N),rb-(rb-lb)/(2*N),N);% Discretize in x
IC=fx(N);

SpatSchem=[-1,1]; %spatial scheme
mid=2; %position of the center of the spatial scheme

%% Spatial discretisation
SystOfEqns=zeros(N,N);

Topo=1:N;
Topo=Topo';
Topol=zeros(N,length(SpatSchem));
for i=1:length(SpatSchem)
    Topol(:,i)=circshift(Topo,N-(i-mid));
end

for i=1:N
    SystOfEqns(i,Topol(i,:))=SpatSchem;
end

%% Time discretisation
% only implicit methods are done for now.

yold=IC;

for first=1:H
    k=(SystOfEqns*yold')';
    ynew=yold-k.*h/n;
    yold=ynew;
end
%hold on
%    plot(x,ynew,'--')
%hold off

%%
% Global variables
lb=-5; % left bound of the domain
rb=5; % right bound of the domain
N=500; % number of cells in x
h=0.001; % timestep
H=7000; %number of time steps

A = [0 -1; -4 0];
[R , D] = eig(A);
Rinv = inv(R);


T=h*H; %final time
n=(rb-lb)/(N); %Cell size

%%

x=linspace(lb+(rb-lb)/(2*N),rb-(rb-lb)/(2*N),N);% Discretize in x
IC1=ones(1,N);
IC2=fx(N);

SpatSchem1=[-1,1]; %spatial scheme
mid1=2; %position of the center of the spatial scheme
SpatSchem2=[-1,1]; %spatial scheme
mid2=2; %position of the center of the spatial scheme

%% Spatial discretisation
SystOfEqns=zeros(N,N);

Topo=1:N;
Topo=Topo';
Topol=zeros(N,length(SpatSchem));
for i=1:length(SpatSchem)
    Topol(:,i)=circshift(Topo,N-(i-mid));
end

for i=1:N
    SystOfEqns1(i,Topol(i,:))=SpatSchem1;
end

for i=1:N
    SystOfEqns2(i,Topol(i,:))=SpatSchem2;
end

%% Time discretisation
% only implicit methods are done for now.

yold1=IC1;

for first=1:H
    k=(SystOfEqns*yold1')';
    ynew1=yold1-k.*h/n;
    yold1=ynew1;
end

yold2=IC2;

for first=1:H
    k=(SystOfEqns*yold2')';
    ynew2=yold2-k.*h/n;
    yold2=ynew2;
end

%hold on
%    plot(x,ynew1,'--')
%hold off

nu=1;
r = 0:pi/50:2*pi;
rho=2*nu.^2*cos(r/2).^2+nu.*sin(r)+1;
circle(0,0,rho)
circle(0,0,1)

function h = circle(x,y,r)
hold on
th = 0:pi/50:2*pi;
xunit = r.*cos(th)+x;
yunit = r.*sin(th)+y;
h = plot(xunit, yunit);
axis equal
hold off
end
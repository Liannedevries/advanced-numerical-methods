% Global variables
clear
close all
lb=-5; % left bound of the domain
rb=5; % right bound of the domain
N=100; % number of cells in x
dx = (rb-lb)/N; % size of cell
Nghost = 1; % number of ghost cells
u = 1;

%% First order upwind method
dx1 = dx;                   % grid size on [-5 0]
dx2 = dx1/2;                % grid size on [0 5]
x1 = (lb+0.5*dx1:dx1:0-0.5*dx1);
x2 = (0.5*dx2:dx2:rb-0.5*dx1);
x = [x1 x2]; % midpoints of the cells
dt=0.05; % timestep. Needs to be chosen s.t. the CFL condition is satisfied for all methods and all grid sizes
CFL = dt/dx2;                   % check CFL condition
Tsteps = ceil(5*(rb-lb)/(dt*u));    % number of time steps s.t. 5 periods are simulated

% initial conditions (value in midpoint cell)
Qext = zeros((Tsteps+1),length(x)+2*Nghost);
Q = zeros((Tsteps+1),length(x));

for i = 1:length(x)             % initial condition for Q
    if x(i)>=-3 && x(i)<=-1
        Q(1,i) = (1-cos(pi*(x(i) - 1)))/2;
    elseif x(i)>=1 && x(i)<=3
        Q(1,i) = 1;
    else
        Q(1,i) = 0;
    end
end

% Implement First order upwind method for advection equation
for k = 2:Tsteps+1
    % defining values for ghost cells
    Qext(k-1,:) = [Q(k-1,length(x)-Nghost+1:length(x)) Q(k-1,:) Q(k-1,1:Nghost)];   
    for i = 1:length(x)
        % select right value for dx
        if x(i)<0
            dxvar = dx1;
        else
            dxvar = dx2;
        end
        Q(k,i) = Qext(k-1,i+Nghost) - (u*dt/dxvar)*(Qext(k-1,i+Nghost)...
            - Qext(k-1,i+Nghost-1));
    end
end

% plot solution after 1, 2 and 5 periods
figure
hold on
stairs(x,Q(1,:))        % plot IC
stairs(x,Q(ceil(1*(rb-lb)/(dt*u)),:))   % plot solution after 1 period
% stairs(x,Q(ceil(2*(rb-lb)/(dt*u)),:))   % plot solution after 2 periods
% stairs(x,Q(ceil(5*(rb-lb)/(dt*u)),:))   % plot solution after 5 periods
title('Advection equation. First order upwind method')
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_upwind_advection = Q(ceil(5*(rb-lb)/(dt*u)),:);
Q_real_advection = Q(1,:);


%% Lax-Wendroff method
dx = 0.1;
dx1 = dx;                   % grid size on [-5 0]
dx2 = dx1/2;                % grid size on [0 5]
x1 = (lb+0.5*dx1:dx1:0-0.5*dx1);
x2 = (0.5*dx2:dx2:rb-0.5*dx1);
x = [x1 x2]; % midpoints of the cells
dt=0.05; % timestep. Needs to be chosen s.t. the CFL condition is satisfied for all methods and all grid sizes
CFL = dt/dx2;                   % check CFL condition
Tsteps = ceil(5*(rb-lb)/(dt*u));    % number of time steps s.t. 5 periods are simulated

% initial conditions (value in midpoint cell)
Q_LWext = zeros((Tsteps+1),length(x)+2*Nghost);
Q_LW = zeros((Tsteps+1),length(x));
Q_LW(1,:) = Q(1,:);                 % copy initial conditions from implementation above

% Implement Lax-Wendroff method for advection equation
for k = 2:Tsteps+1
    % defining values for ghost cells
    Q_LWext(k-1,:) = [Q_LW(k-1,length(x)-Nghost+1:length(x)) Q_LW(k-1,:)...
        Q_LW(k-1,1:Nghost)];   
    for i = 1:length(x)
        % select right value for dx
        if x(i)<0
            dxvar = dx1;
        else
            dxvar = dx2;
        end
    Q_LW(k,i) = Q_LWext(k-1,i+Nghost) - ((dt*u)/(2*dxvar))*...
        (Q_LWext(k-1,i+1+Nghost) - Q_LWext(k-1,i-1+Nghost)) +...
        0.5*((dt/dxvar)^2)*u^2*(Q_LWext(k-1,i-1+Nghost)...
    - 2*Q_LWext(k-1,i+Nghost) + Q_LWext(k-1,i+1+Nghost));
    end
end

% % animate solution
% for k = 1:Tsteps+1
%     stairs(x,Q_LW(k,:),'b')
%     pause(0.01)
% end

% plot solution after 1, 2 and 5 periods
figure
hold on
stairs(x,Q_LW(1,:))        % plot IC
stairs(x,Q_LW(ceil(1*(rb-lb)/(dt*u)),:))   % plot solution after 1 period
stairs(x,Q_LW(ceil(2*(rb-lb)/(dt*u)),:))   % plot solution after 2 periods
stairs(x,Q_LW(ceil(5*(rb-lb)/(dt*u)),:))   % plot solution after 5 periods
title('Advection equation. Lax-Wendroff method')
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_LW_advection = Q_LW(ceil(5*(rb-lb)/(dt*u))+1,:);
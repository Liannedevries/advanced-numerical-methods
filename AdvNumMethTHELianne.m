% Global variables
clear
close all
lb=-5;              % Left bound of the domain.
rb=5;               % Right bound of the domain.
N=100;              % Number of cells in x.
dx = (rb-lb)/N;     % Size of cell.
Nghost = 2;         % Number of ghost cells. Min 2 for flux limiter method.
u = 1;
A = [0 -1; -4 0];
x=linspace(lb+(rb-lb)/(2*N),rb-(rb-lb)/(2*N),N);% Discretize x spatial.

%% Advection equation, First order upwind.
dt=0.05;                         % Timestep, mind the CFL condition.
Tsteps = ceil(5*(rb-lb)/(dt*u)); % Number of time steps, simulates 5 periods.

% I.C. {Initial Conditions} (value in midpoint cell).
Q = zeros(Tsteps+1,N);
Qext = zeros(Tsteps+1,N+2*Nghost);
Q(1,:) = fx(N);

% Implement First order upwind method for advection equation.
for k = 2:Tsteps+1
    % defining values for ghost cells.
    Qext(k-1,:) = [Q(k-1,N-Nghost+1:N) Q(k-1,:) Q(k-1,1:Nghost)];   
    for i = 1:N
        Q(k,i) = Qext(k-1,i+Nghost) - (u*dt/dx)*(Qext(k-1,i+Nghost)...
            - Qext(k-1,i+Nghost-1));
    end
end

%% animate solution
% for k = 1:Tsteps+1
%     stairs(x,Q(k,:),'b')
%     pause(0.01)
% end

%% plot solution after 1, 2 and 5 periods
figure
hold on
stairs(x,Q(1,:))                        % plot IC.
stairs(x,Q(ceil(1*(rb-lb)/(dt*u)),:))   % plot solution after 1 period.
stairs(x,Q(ceil(2*(rb-lb)/(dt*u)),:))   % plot solution after 2 periods.
stairs(x,Q(ceil(5*(rb-lb)/(dt*u)),:))   % plot solution after 5 periods.
title('Advection equation. First order upwind method')
legend('Initial conditions','After 1 period','After 2 periods',....
    'After 5 periods','Location','northwest')
Q_upwind_advection = Q(ceil(5*(rb-lb)/(dt*u)),:);
Q_real_advection = Q(1,:);

%% Stress equations, First order upwind.
dt = 0.025;                      % Timestep, mind the CFL condition.
Tsteps = ceil(5*(rb-lb)/(dt*2)); % Number of time steps, simulates 5 periods.

% Decouple system and inverse.
[R , D] = eig(A);
Rinv = inv(R);

% I.C.
S = zeros(2*(Tsteps+1),N);
W1 = zeros((Tsteps+1),N);
W2 = zeros((Tsteps+1),N);

S(1,:) = 1;                      % I.C. for sigma
S(2,:) = fx(N);                  % I.C. for v

W = Rinv*S(1:2,:);
W1(1,:) = W(1,:);
W2(1,:) = W(2,:);

% Implement first order upwind method.
Q = W1;
u = D(1,1);
for k = 2:Tsteps+1
    % Assigning ghost cell values.
    Qext(k-1,:) = [Q(k-1,N-Nghost+1:N) Q(k-1,:) Q(k-1,1:Nghost)];   
    for i = 1:N
        Q(k,i) = Qext(k-1,i+Nghost) - (u*dt/dx)*(Qext(k-1,i+Nghost)...
            - Qext(k-1,i+Nghost-1));
    end
end
W1 = Q;

Q = W2;
u = D(2,2);
for k = 2:Tsteps+1
    % Assigning ghost cell values.
    Qext(k-1,:) = [Q(k-1,N-Nghost+1:N) Q(k-1,:) Q(k-1,1:Nghost)];   
    for i = 1:N
        Q(k,i) = Qext(k-1,i+Nghost) + (u*dt/dx)*(Qext(k-1,i+Nghost)...
            - Qext(k-1,i+Nghost+1));
    end
end
W2 = Q;

% construct S from W
for k = 2:Tsteps+1
    for i = 1:N
        S(2*k-1:2*k,i) = W1(k,i)*R(:,1) + W2(k,i)*R(:,2);
    end
end

%% animate solutions
% figure
% s(1) = subplot(2,2,1);
% s(2) = subplot(2,2,2);
% s(3) = subplot(2,2,3); 
% s(4) = subplot(2,2,4); 
% 
% for k = 1:Tsteps+1
%     stairs(s(1),x,W1(k,:),'b')
%     stairs(s(3),x,W2(k,:),'b')
%     stairs(s(2),x,S(2*k-1,:),'b')
%     stairs(s(4),x,S(2*k,:),'b')
%     pause(0.01)
% end

%% plot solution after 1, 2 and 5 periods
figure
title('Stress equations. First order upwind method')
s(1) = subplot(2,1,1);
hold on
stairs(s(1),x,S(1,:))                                 % Plot IC.
stairs(s(1),x,S(1+2*ceil(1*(rb-lb)/(dt*D(1,1))),:))   % Plot 1 period.
stairs(s(1),x,S(1+2*ceil(2*(rb-lb)/(dt*D(1,1))),:))   % Plot 2 periods.
stairs(s(1),x,S(1+2*ceil(5*(rb-lb)/(dt*D(1,1))),:))   % Plot 5 periods.
title('Stress equations. First order upwind method')
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_upwind_stress(1,:) = S(1+2*ceil(5*(rb-lb)/(dt*D(1,1))),:);
s(2) = subplot(2,1,2);
hold on
stairs(s(2),x,S(2,:))                               % Plot IC.
stairs(s(2),x,S(2*ceil(1*(rb-lb)/(dt*D(1,1))),:))   % Plot 1 period.
stairs(s(2),x,S(2*ceil(2*(rb-lb)/(dt*D(1,1))),:))   % Plot 2 periods.
stairs(s(2),x,S(2*ceil(5*(rb-lb)/(dt*D(1,1))),:))   % Plot 5 periods.
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_upwind_stress(2,:) = S(2*ceil(5*(rb-lb)/(dt*D(1,1))),:);
Q_real_stress = S(1:2,:);

%% Advection equation, Lax-Wendroff
dt = 0.05;              % mind the CFL condition
u = 1;                  % set u again to value of advection equation
Tsteps = ceil(5*(rb-lb)/(dt*u)); % Number of time steps, simulates 5 periods.
% I.C.
Q_LW = zeros(Tsteps+1,N);
Q_LWext = zeros(Tsteps+1,N+2*Nghost);
Q_LW(1,:) = fx(N);

% Implement Lax-Wendroff method for advection equation.
for k = 2:Tsteps+1
    % Assigning ghost cells values.
    Q_LWext(k-1,:) = [Q_LW(k-1,N-Nghost+1:N) Q_LW(k-1,:) Q_LW(k-1,1:Nghost)];   
    for i = 1:N
    Q_LW(k,i) = Q_LWext(k-1,i+Nghost) - ((dt*u)/(2*dx))*(Q_LWext(k-1,i+1+Nghost)...
    - Q_LWext(k-1,i-1+Nghost)) + 0.5*((dt/dx)^2)*u^2*(Q_LWext(k-1,i-1+Nghost)...
    - 2*Q_LWext(k-1,i+Nghost) + Q_LWext(k-1,i+1+Nghost));
    end
end

%% animate solution
% for k = 1:Tsteps+1
%     stairs(x,Q_LW(k,:),'b')
%     pause(0.01)
% end

%% plot solution after 1, 2 and 5 periods
figure
hold on
stairs(x,Q_LW(1,:))                        % Plot IC.
stairs(x,Q_LW(ceil(1*(rb-lb)/(dt*u)),:))   % Plot solution after 1 period.
stairs(x,Q_LW(ceil(2*(rb-lb)/(dt*u)),:))   % Plot solution after 2 periods.
stairs(x,Q_LW(ceil(5*(rb-lb)/(dt*u)),:))   % Plot solution after 5 periods.
title('Advection equation. Lax-Wendroff method')
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_LW_advection = Q_LW(ceil(5*(rb-lb)/(dt*u))+1,:);

%% Stress equations
dt = 0.025;                      % Timestep, mind the CFL condition.
Tsteps = ceil(5*(rb-lb)/(dt*2)); % Number of time steps, simulates 5 periods.
% Determine eigenvalues to get wave speeds
[~ , D] = eig(A);

% I.C.
S_LW = zeros(2*(Tsteps+1),N);
SLW_ext = zeros(2*(Tsteps+1),N+2*Nghost);

S_LW(1,:) = 1;                     % I.C. for sigma.
S_LW(2,:) = fx(N);                 % I.C. for v.

% Implement Lax-Wendroff method for advection equation.
Q_LW = S_LW;
for k = 2:Tsteps+1
    % Assigning ghost cells values.
    Q_LWext(2*k-3:2*k-2,:) = [Q_LW(2*k-3:2*k-2,N-Nghost+1:N)...
        Q_LW(2*k-3:2*k-2,:) Q_LW(2*k-3:2*k-2,1:Nghost)];   
    for i = 1:N
    Q_LW(2*k-1:2*k,i) = Q_LWext(2*k-3:2*k-2,i+Nghost) - ...
        ((dt*A)/(2*dx))*(Q_LWext(2*k-3:2*k-2,i+1+Nghost)...
        - Q_LWext(2*k-3:2*k-2,i-1+Nghost)) + ...
        0.5*((dt/dx)^2)*A^2*(Q_LWext(2*k-3:2*k-2,i-1+Nghost)...
    - 2*Q_LWext(2*k-3:2*k-2,i+Nghost) + Q_LWext(2*k-3:2*k-2,i+1+Nghost));
    end
end
S_LW = Q_LW;


%% animate solutions
% figure
% s(1) = subplot(2,2,1);
% s(2) = subplot(2,2,2);
% s(3) = subplot(2,2,3); 
% s(4) = subplot(2,2,4); 
% 
% for k = 1:Tsteps+1
%     stairs(s(1),x,S(2*k-1,:),'b')
%     stairs(s(3),x,S(2*k,:),'b')
%     stairs(s(2),x,S_LW(2*k-1,:),'b')
%     stairs(s(4),x,S_LW(2*k,:),'b')
%     pause(0.01)
% end

%% plot solution after 1, 2 and 5 periods
figure
title('Stress equations. Lax-Wendroff method')
s(1) = subplot(2,1,1);
hold on
stairs(s(1),x,S_LW(1,:))                               % Plot I.C.
stairs(s(1),x,S_LW(1+2*ceil(1*(rb-lb)/(dt*D(1,1))),:)) % Plot 1 period.
stairs(s(1),x,S_LW(1+2*ceil(2*(rb-lb)/(dt*D(1,1))),:)) % Plot 2 periods.
stairs(s(1),x,S_LW(1+2*ceil(5*(rb-lb)/(dt*D(1,1))),:)) % Plot 5 periods.
axis([-5 5 0.8 1.3])
title('Stress equations. Lax-Wendroff method')
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_LW_stress(1,:) = S_LW(1+2*ceil(5*(rb-lb)/(dt*D(1,1))),:);
s(2) = subplot(2,1,2);
hold on
stairs(s(2),x,S_LW(2,:))        % plot IC
stairs(s(2),x,S_LW(2*ceil(1*(rb-lb)/(dt*D(1,1))),:))   % Plot 1 period.
stairs(s(2),x,S_LW(2*ceil(2*(rb-lb)/(dt*D(1,1))),:))   % Plot 2 periods.
stairs(s(2),x,S_LW(2*ceil(5*(rb-lb)/(dt*D(1,1))),:))   % Plot 5 periods.
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_LW_stress(2,:) = S_LW(2*ceil(5*(rb-lb)/(dt*D(1,1))),:);

%% Advection equation, MC-flux limiter
dt = 0.025;                      % Timestep, mind the CFL condition.
u = 1;                           % setting u to right value for advection equation
Tsteps = ceil(5*(rb-lb)/(dt*u)); % Number of time steps, simulates 5 periods.

% I.C.
Q = zeros(Tsteps+1,N);
Qext = zeros(Tsteps+1,N+2*Nghost);
Q(1,:) = fx(N);

% Implement MC-flux limiter method for advection equation
for k = 2:Tsteps+1
    % Assigning ghost cells values.
    Qext(k-1,:) = [Q(k-1,N-Nghost+1:N) Q(k-1,:) Q(k-1,1:Nghost)];   
    for i = 1:N
        Q(k,i) = Qext(k-1,i+Nghost) - dt/dx*(...
            u*(Qext(k-1,i+Nghost) - Qext(k-1,i-1+Nghost))...
            +0.5*u*(1 - u*dt/dx)*...
            (MCfluxpos(Qext(k-1,i-1+Nghost:i+1+Nghost)) * (Qext(k-1,i+1+Nghost)-...
            Qext(k-1,i+Nghost)) - MCfluxpos(Qext(k-1,i-2+Nghost:i+Nghost)) *...
            (Qext(k-1,i+Nghost) - Qext(k-1,i-1+Nghost))));
    end
end

%% animate solution
% for k = 1:Tsteps+1
%     stairs(x,Q(k,:),'b')
%     pause(0.001)
% end

%% plot solution after 1, 2 and 5 periods
figure
hold on
stairs(x,Q(1,:))                        % plot I.C.
stairs(x,Q(ceil(1*(rb-lb)/(dt*u)),:))   % plot solution after 1 period.
stairs(x,Q(ceil(2*(rb-lb)/(dt*u)),:))   % plot solution after 2 periods.
stairs(x,Q(ceil(5*(rb-lb)/(dt*u)),:))   % plot solution after 5 periods.
title('Advection equation. MC-flux limiter method')
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_MC_advection = Q(ceil(5*(rb-lb)/(dt*u)),:);

%% Stress equations, MC-flux limiter
dt = 0.025;                      % Timestep, mind the CFL condition.
Tsteps = ceil(5*(rb-lb)/(dt*2)); % Number of time steps, simulates 5 periods.
% Decouple system.
[R , D] = eig(A);
Rinv = inv(R);  

% initial conditions
S = zeros(2*(Tsteps+1),N);
% Sext = zeros(2*(Tsteps+1),N+2*Nghost);
% Wext = zeros(2*(Tsteps+1),N+2*Nghost);
W1 = zeros((Tsteps+1),N);
W2 = zeros((Tsteps+1),N);
W1ext = zeros((Tsteps+1),N+2*Nghost);
W2ext = zeros((Tsteps+1),N+2*Nghost);

S(1,:) = 1;                     % IC for sigma
S(2,:) = fx(N);                 % initial condition for v

W = Rinv*S(1:2,:);
W1(1,:) = W(1,:);
W2(1,:) = W(2,:);

Q = W1;
u = D(1,1);
% Implement MC-flux limiter method for stress equations
for k = 2:Tsteps+1
    % defining values for ghost cells
    Qext(k-1,:) = [Q(k-1,N-Nghost+1:N) Q(k-1,:) Q(k-1,1:Nghost)];   
    for i = 1:N
        Q(k,i) = Qext(k-1,i+Nghost) - dt/dx*(...
            u*(Qext(k-1,i+Nghost) - Qext(k-1,i-1+Nghost))...
            +0.5*u*(1 - u*dt/dx)*...
            (MCfluxpos(Qext(k-1,i-1+Nghost:i+1+Nghost)) * (Qext(k-1,i+1+Nghost)-...
            Qext(k-1,i+Nghost)) - MCfluxpos(Qext(k-1,i-2+Nghost:i+Nghost)) *...
            (Qext(k-1,i+Nghost) - Qext(k-1,i-1+Nghost))));
    end
end
W1 = Q;

Q = W2;
u = D(2,2);
% Implement MC-flux limiter method for advection equation
for k = 2:Tsteps+1
    % defining values for ghost cells
    Qext(k-1,:) = [Q(k-1,N-Nghost+1:N) Q(k-1,:) Q(k-1,1:Nghost)];   
    for i = 1:N
        Q(k,i) = Qext(k-1,i+Nghost) + dt/dx*(...
            u*(Qext(k-1,i+Nghost) - Qext(k-1,i+1+Nghost))...
            +0.5*abs(u)*(1 - abs(u*dt/dx))*...
            (MCfluxneg(Qext(k-1,i-1+Nghost:i+1+Nghost)) * (Qext(k-1,i+Nghost)-...
            Qext(k-1,i-1+Nghost)) - MCfluxneg(Qext(k-1,i+Nghost:i+2+Nghost)) *...
            (Qext(k-1,i+1+Nghost) - Qext(k-1,i+Nghost))));
    end
end
W2 = Q;

% construct S from W
for k = 2:Tsteps+1
    for i = 1:N
        S(2*k-1:2*k,i) = W1(k,i)*R(:,1) + W2(k,i)*R(:,2);
    end
end

%% animate solutions
% figure
% s(1) = subplot(2,2,1);
% s(2) = subplot(2,2,2);
% s(3) = subplot(2,2,3); 
% s(4) = subplot(2,2,4); 
% 
% for k = 1:Tsteps+1
%     stairs(s(1),x,W1(k,:),'b')
%     stairs(s(3),x,W2(k,:),'b')
%     stairs(s(2),x,S(2*k-1,:),'b')
%     stairs(s(4),x,S(2*k,:),'b')
%     pause(0.001)
% end

%% plot solution after 1, 2 and 5 periods
figure
s(1) = subplot(2,1,1);
title('Stress equations. MC-flux limiter method')
hold on
stairs(s(1),x,S(1,:))                                 % plot I.C.
stairs(s(1),x,S(1+2*ceil(1*(rb-lb)/(dt*D(1,1))),:))   % plot 1 period.
stairs(s(1),x,S(1+2*ceil(2*(rb-lb)/(dt*D(1,1))),:))   % plot 2 periods.
stairs(s(1),x,S(1+2*ceil(5*(rb-lb)/(dt*D(1,1))),:))   % plot 5 periods.
axis([-5 5 0.99999999999995 1.0000000000002])
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_MC_stress(1,:) = S(1+2*ceil(5*(rb-lb)/(dt*D(1,1))),:);
s(2) = subplot(2,1,2);
hold on
stairs(s(2),x,S(2,:))                               % plot I.C.
stairs(s(2),x,S(2*ceil(1*(rb-lb)/(dt*D(1,1))),:))   % plot 1 period.
stairs(s(2),x,S(2*ceil(2*(rb-lb)/(dt*D(1,1))),:))   % plot 2 periods.
stairs(s(2),x,S(2*ceil(5*(rb-lb)/(dt*D(1,1))),:))   % plot 5 periods.
legend('Initial conditions','After 1 period','After 2 periods',...
    'After 5 periods','Location','northwest')
Q_MC_stress(2,:) = S(2*ceil(5*(rb-lb)/(dt*D(1,1))),:);

%% plot real solutions and compare with numerical solutions, plot error
figure
s(1) = subplot(2,1,1);
hold on
title('Advection equation: Comparison of all three methods after 5 periods')
stairs(s(1),x,Q_real_advection)
stairs(s(1),x,Q_upwind_advection)
stairs(s(1),x,Q_LW_advection)
stairs(s(1),x,Q_MC_advection)
legend('Initial conditions','First order upwind method',...
    'Lax-Wendroff method','MC flux limiter','Location','northwest')

s(2) = subplot(2,1,2);
hold on
title('Advection equation: Comparison of the error after 5 periods')
stairs(s(2),x,Q_real_advection-Q_upwind_advection)
stairs(s(2),x,Q_real_advection-Q_LW_advection)
stairs(s(2),x,Q_real_advection-Q_MC_advection)
legend('First order upwind method','Lax-Wendroff method',...
    'MC flux limiter','Location','northwest')

figure
s(1) = subplot(2,1,1);
hold on
title('Stress equations 1: Comparison of all three methods after 5 periods')
stairs(s(1),x,Q_real_stress(1,:))
stairs(s(1),x,Q_upwind_stress(1,:))
stairs(s(1),x,Q_LW_stress(1,:))
stairs(s(1),x,Q_MC_stress(1,:))
legend('Initial conditions','First order upwind method',...
    'Lax-Wendroff method','MC flux limiter','Location','northwest')
axis([-5 5 0.85 1.2])

s(2) = subplot(2,1,2);
hold on
title('Stress equations 1: Comparison of the error after 5 periods')
stairs(s(2),x,Q_real_stress(1,:)-Q_upwind_stress(1,:))
stairs(s(2),x,Q_real_stress(1,:)-Q_LW_stress(1,:))
stairs(s(2),x,Q_real_stress(1,:)-Q_MC_stress(1,:))
legend('First order upwind method','Lax-Wendroff method',...
    'MC flux limiter','Location','northwest')

figure
s(1) = subplot(2,1,1);
hold on
title('Stress equations 2: Comparison of all three methods after 5 periods')
stairs(s(1),x,Q_real_stress(2,:))
stairs(s(1),x,Q_upwind_stress(2,:))
stairs(s(1),x,Q_LW_stress(2,:))
stairs(s(1),x,Q_MC_stress(2,:))
legend('Initial conditions','First order upwind method',...
    'Lax-Wendroff method','MC flux limiter','Location','northwest')

s(2) = subplot(2,1,2);
hold on
title('Stress equations 2: Comparison of the error after 5 periods')
stairs(s(2),x,Q_real_stress(2,:)-Q_upwind_stress(2,:))
stairs(s(2),x,Q_real_stress(2,:)-Q_LW_stress(2,:))
stairs(s(2),x,Q_real_stress(2,:)-Q_MC_stress(2,:))
legend('First order upwind method','Lax-Wendroff method',...
    'MC flux limiter','Location','northwest')

%% define function for the MC_flux limiter as a function of Q at interface
% i-1/2
function phipos = MCfluxpos(Qflux)        
% input is a vector [Q(i-2) Q(i-1) Q(i)] with Q at time n
if Qflux(3) - Qflux(2) == 0
    theta = 1000;
else
theta = (Qflux(2) - Qflux(1))/(Qflux(3) - Qflux(2));
end
arg2 = min([(1+theta)/2 2 2*theta]);
phipos = max([0 arg2]);
end

function phineg = MCfluxneg(Qflux)        
% input is a vector [Q(i-1) Q(i) Q(i+1)] with Q at time n
if Qflux(2) - Qflux(1) == 0
    theta = 1000;
else
theta = (Qflux(3) - Qflux(2))/(Qflux(2) - Qflux(1));
end
arg2 = min([(1+theta)/2 2 2*theta]);
phineg = max([0 arg2]);
end
%% generate I.C.
function [f] = fx(n)
% generates the initial condition as described in the assignment
f=linspace(-5+(10)/(2*n),5-(10)/(2*n),n);
for i=1:length(f)
    if f(i)>-3 && f(i)<-1
        f(i)=0.5*(1-cos(pi*(f(i)-1)));
    elseif f(i)>1 && f(i)<3
        f(i)=1;
    else
        f(i)=0;
    end
end 
end

